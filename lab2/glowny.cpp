#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <climits>
#include <list>
#include <ctime>

using namespace std;

#define N 100 // dlugosc tablic

const string nazwa[11] = {"data10.txt", "data11.txt", "data12.txt", "data13.txt", "data14.txt",
 "data15.txt", "data16.txt", "data17.txt", "data18.txt", "data19.txt", "data20.txt"}; // nazwa pliku z danymi
clock_t start = 0;
clock_t suma_czas = 0;  

/*
W liczbie zadania_do_wykonania s� zapisane binarnie zadania ktore zostan� wykonane.

p to tablica z czasami przetwarzania

Np. 0000101 oznacza ze zostanie wykonane zadanie nr 0 i 2
*/
unsigned oblicz_czas_wykonywania(unsigned p[], unsigned zadania_do_wykonania, unsigned liczba_zadan){
	unsigned retval = 0;
	for(unsigned i = 0; i < liczba_zadan; i++){
		if(zadania_do_wykonania & (1 << i)){
			retval += p[i];
		} // if
	} // for
	return retval;
}



int main(){
	
	/*
		Uwaga! Zadania numerowane od zera w��cznie!
	*/
	
	for(int inst=0; inst<=10; inst++)
	{
	unsigned p[N], w[N], d[N];
	unsigned n; // liczba operacji
	
	cout << endl << nazwa[inst] << endl;
		
	fstream plik(nazwa[inst].c_str());
	
	plik >> n;
	vector<unsigned> F(1<<n);
	vector<int> tab_ostatnich(1<<n);
	
	for(unsigned i = 0; i < n; i++){
		plik >> p[i]; 
		plik >> w[i];
		plik >> d[i];
	} // for
	
	plik.close();
	
	start = clock();
	F[0] = 0;
	tab_ostatnich[0] = -1;
	for(unsigned i = 1; i < (1<<n); i++){
		unsigned min_sum = UINT_MAX;
		unsigned index_ostatniego;
		for(unsigned j = 0; j < n; j++){ // w najgorszym wypadku trzeba sprawdzic n bitow
			if(i & (1<<j)){ // sprawdzamy czy dana operacja sklada sie na sekwencje
				// kara_dla_ostatniego = waga_dla_ostatniego * (deadline - (czas_wykonywania_poprzednich + czas_wykonywania_ostatniego))
				unsigned czas_wykonywania = oblicz_czas_wykonywania(p,i, n);
				unsigned kara_dla_ostatniego = (d[j] >= czas_wykonywania) ? 0 : (w[j] * (czas_wykonywania - d[j]));
				unsigned pot_min_sum = F[i & ~(1<<j)] + kara_dla_ostatniego;
				if(pot_min_sum < min_sum){
					min_sum = pot_min_sum;
					index_ostatniego = j;
				} // if
			} // if
		} // for
		F[i] = min_sum;
		tab_ostatnich[i] = index_ostatniego;
	} // for
		
	cout << "suma Optimum:" << endl << F[F.size()-1] << endl;
	
	list<int> roz;
	unsigned i = tab_ostatnich.size() - 1;
	while(i>0){
		roz.push_front(tab_ostatnich[i]);
		i &= ~(1<<tab_ostatnich[i]);
	}
	
	unsigned suma_p = 0;
	unsigned suma_kar = 0;
	for (std::list<int>::iterator it=roz.begin(); it != roz.end(); ++it){
		// prowadzacy numeruje zadania od 1 a nie jak my od zera
		cout << ((*it) + 1) << " ";
		// sprawdzamy tez czy nasza permutacja na pewno jest w porzadku
		suma_p += p[*it];
		suma_kar += (d[*it] >= suma_p) ? 0 : (w[*it] * (suma_p - d[*it]));
	}
	cout << endl;
		
	if(suma_kar != F[F.size()-1]){
		cerr << "Wylapano blad przy sprawdzaniu permutacji" << endl;
	}
	
	suma_czas = suma_czas + (clock() - start);
	
	}//for
		
	cout << "\nLaczny czas pracy algorytmu dla wszystkich instancji: " << suma_czas << " ms";	
	
	return 0;
}
