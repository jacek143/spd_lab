#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

unsigned insa(unsigned LiczbaMaszyn, vector<vector<unsigned> > Operacje, vector<unsigned> * pSekwencjaWynikowa);
bool porownaj_czasy(vector<unsigned> Op1, vector<unsigned> Op2);

unsigned LiczbaMaszyn;
unsigned LiczbaOperacji;

int main(int argc, char *argv[])
{
	const string NazwaPliku = "insa.data.txt";
	const unsigned LiczbaPrzykladow = 1;
	fstream Plik;

	string Smietnik;
	vector<vector<unsigned> > Operacje;
	unsigned PrawidlowyCMax;
	vector<unsigned> PrawidlowaSekwencja;
	
	
	Plik.open(NazwaPliku.c_str());
	
	for(unsigned i = 0; i < LiczbaPrzykladow; i++){
		Operacje.clear();
		Plik >> Smietnik;
		Plik >> LiczbaMaszyn;
		Plik >> LiczbaMaszyn;
		Plik >> LiczbaOperacji;
		
		int it=0;
		for(unsigned j = 0; j < LiczbaMaszyn; j++){
			Plik >> Smietnik;
			for(unsigned k = 0; k < LiczbaMaszyn; k++){ // w naszym przypadku zada� jest tyle samo co maszyn
				unsigned NrMaszyny, Czas;
				Plik >> NrMaszyny;
				Plik >> Czas;
				vector<unsigned> Operacja(3,0);
				Operacja[0] = it+1; // nr zadania
				Operacja[1] = NrMaszyny;
				Operacja[2] = Czas;
				Operacje.push_back(Operacja);
				it++;
			} // for
		} // for
		
		vector<unsigned> SekwencjaWynikowa;
		unsigned OtrzymanyCMax = insa(LiczbaMaszyn, Operacje, & SekwencjaWynikowa);
		
		Plik >> Smietnik;
		Plik >> PrawidlowyCMax;
		PrawidlowaSekwencja.clear();
		for(unsigned j = 0; j < (LiczbaMaszyn * LiczbaMaszyn); j++){
			unsigned NrOperacji;
			Plik >> NrOperacji;
			PrawidlowaSekwencja.push_back(NrOperacji);
		} // for
		
		cout << endl;
		cout << "insa: " ;
		cout << OtrzymanyCMax << endl;
		for(unsigned j = 0; j < SekwencjaWynikowa.size(); j++){
			if((j%LiczbaMaszyn == 0) && j != 0) cout << endl;
			cout << SekwencjaWynikowa[j] << "\t";
		} // for
		cout << endl;
		
	} // for
	
	Plik.close();
	system("pause");
	return 0;
}

bool porownaj_czasy(vector<unsigned> Op1, vector<unsigned> Op2){
	return Op1[2] > Op2[2];
}

void nast_pop_tech(vector<vector<unsigned> > Operacje, unsigned NT[], unsigned PT[])
{
	for(int i=0; i<(LiczbaOperacji); i++)
	{
		if(i%LiczbaMaszyn==0) PT[i] = 0;
		else PT[i] = Operacje[i-1][0];
		if(i%LiczbaMaszyn==LiczbaMaszyn-1) NT[i] = 0;
		else NT[i] = Operacje[i+1][0];
	}
}

void liczba_poprzednikow(unsigned PT[], unsigned PK[], unsigned LP[])
{
	for(unsigned i=0; i<LiczbaOperacji; i++)
	{
		unsigned j=0;
		if(PT[i]!=0) j++;
		if(PK[i]!=0) j++;
		LP[i]=j;
	}
}

void kolejnosc_topologiczna(unsigned LP[], unsigned PT[], unsigned PK[], unsigned KT[])
{
	unsigned j=0, l=0;
	unsigned pomLP[LiczbaOperacji], pomPT[LiczbaOperacji], pomPK[LiczbaOperacji];
	for(unsigned i=0; i<LiczbaOperacji; i++)
	{
		pomLP[i]=LP[i]; pomPT[i]=PT[i]; pomPK[i]=PK[i];
	}
	vector <unsigned> bylo(LiczbaOperacji, 0);
	while(KT[LiczbaOperacji-1]==0) {
		l=j;
		for(unsigned i=0; i<LiczbaOperacji; i++) {
			if(pomLP[i]==0 && bylo[i]==0) {
				KT[j]=i+1;
				bylo[i]=1;
				j++;
			}
		}
		for(unsigned i=l; i<j; i++) {
			pomPT[KT[i]]=0;		
		}
		for(unsigned i=0; i<LiczbaOperacji; i++) {
			if(pomLP[i]!=0) {
				for(unsigned k=l; k<j; k++) {
					if(KT[k]==pomPK[i]) pomPK[i]=0;
				}
			}
		}
		liczba_poprzednikow(pomPT, pomPK, pomLP);
	}//while
}

void licz_R(unsigned R[], unsigned PT[], unsigned PK[], unsigned KT[], vector<vector<unsigned> > OperacjeNaturalna)
{
	for(unsigned i=0; i<LiczbaOperacji; i++)
	{
		if(PT[KT[i]-1]==0 && PK[KT[i]-1]==0) R[KT[i]-1] = OperacjeNaturalna[KT[i]-1][2];
		if(PK[KT[i]-1]!=0 && PT[KT[i]-1]!=0) R[KT[i]-1] = max(R[PT[KT[i]-1]-1], R[PK[KT[i]-1]-1]) + OperacjeNaturalna[KT[i]-1][2];
		if(PK[KT[i]-1]==0 && PT[KT[i]-1]!=0) R[KT[i]-1] = R[PT[KT[i]-1]-1] + OperacjeNaturalna[KT[i]-1][2];
		if(PK[KT[i]-1]!=0 && PT[KT[i]-1]==0) R[KT[i]-1] = R[PK[KT[i]-1]-1] + OperacjeNaturalna[KT[i]-1][2];
	}
}

void licz_Q(unsigned Q[], unsigned NT[], unsigned NK[], unsigned KT[], vector<vector<unsigned> > OperacjeNaturalna)
{
	for(unsigned i=LiczbaOperacji-1; i<LiczbaOperacji; i--)
	{
		if(NT[KT[i]-1]==0 && NK[KT[i]-1]==0) Q[KT[i]-1] = OperacjeNaturalna[KT[i]-1][2];
		if(NK[KT[i]-1]!=0 && NT[KT[i]-1]!=0) Q[KT[i]-1] = max(Q[NT[KT[i]-1]-1], Q[NK[KT[i]-1]-1]) + OperacjeNaturalna[KT[i]-1][2];
		if(NK[KT[i]-1]==0 && NT[KT[i]-1]!=0) Q[KT[i]-1] = Q[NT[KT[i]-1]-1] + OperacjeNaturalna[KT[i]-1][2];
		if(NK[KT[i]-1]!=0 && NT[KT[i]-1]==0) Q[KT[i]-1] = Q[NK[KT[i]-1]-1] + OperacjeNaturalna[KT[i]-1][2];
	}
}

unsigned insa(unsigned LiczbaMaszyn, vector<vector<unsigned> > Operacje, vector<unsigned> * SekwencjaWynikowa){
	unsigned NT[LiczbaOperacji];
	unsigned PT[LiczbaOperacji];
	unsigned NK[LiczbaOperacji];
	unsigned PK[LiczbaOperacji];
	unsigned LP[LiczbaOperacji];
	unsigned R[LiczbaOperacji];
	unsigned Q[LiczbaOperacji];
	unsigned KT[LiczbaOperacji];
	unsigned ktory_z_kolei=0;
	unsigned ktora_maszyna;
	vector<vector<unsigned> > Sekwencja;
	for(unsigned i=0; i<LiczbaMaszyn; i++)
	{
		vector<unsigned> pom;
		Sekwencja.push_back(pom);
	} 
	vector<vector<unsigned> > OperacjeNaturalna;
	OperacjeNaturalna = Operacje;
	for(unsigned i=0; i<LiczbaOperacji; i++){
		NK[i]=0;
		PK[i]=0;
	}
	nast_pop_tech(Operacje, NT, PT);
	liczba_poprzednikow(PT, PK, LP);
	
	KT[LiczbaOperacji-1]=0;
	kolejnosc_topologiczna(LP, PT, PK, KT);
	licz_R(R, PT, PK, KT, OperacjeNaturalna);
	licz_Q(Q, NT, NK, KT, OperacjeNaturalna);
	
	sort(Operacje.begin(), Operacje.end(), porownaj_czasy);

	for(unsigned i=0; i<LiczbaOperacji; i++)
	{
		vector<unsigned> zadanie;
		zadanie=Operacje[ktory_z_kolei];
		ktory_z_kolei++;
		ktora_maszyna=zadanie[1]-1;
		if(Sekwencja[ktora_maszyna].empty())
		{
			Sekwencja[ktora_maszyna].push_back(zadanie[0]);
		}
		else
		{
			unsigned pk, npk, nk, nnk, c=100000, temp, nj;
			for(unsigned j=0; j<=Sekwencja[ktora_maszyna].size(); j++)
			{
				if(j==0) pk = 0;
				else pk=Sekwencja[ktora_maszyna][j-1];
				if(j==Sekwencja[ktora_maszyna].size()) nk = 0;
				else nk=Sekwencja[ktora_maszyna][j];
				
				if(zadanie[0]==1)
				{
					if(pk==0 && nk!=0) temp = zadanie[2] + max(Q[nk-1],Q[NT[zadanie[0]-1]-1]);
					if(pk!=0 && nk!=0) temp = R[pk-1] + zadanie[2] + max(Q[nk-1],Q[NT[zadanie[0]-1]-1]);
					if(pk!=0 && nk==0) temp = R[pk-1] + zadanie[2] + Q[NT[zadanie[0]-1]-1];
				}
				else
				{
					if(pk!=0 && nk!=0) temp = max(R[pk-1],R[PT[zadanie[0]-1]-1]) + zadanie[2] + max(Q[nk-1],Q[NT[zadanie[0]-1]-1]);
					if(pk==0 && nk!=0) temp = R[PT[zadanie[0]-1]-1] + zadanie[2] + max(Q[nk-1],Q[NT[zadanie[0]-1]-1]);
					if(pk!=0 && nk==0) temp = max(R[pk-1],R[PT[zadanie[0]-1]-1]) + zadanie[2] + Q[NT[zadanie[0]-1]-1];
				}
				if(temp<c) 
				{
					c = temp;
					nnk = nk;
					npk = pk;
					nj = j;
				}
			}
			Sekwencja[ktora_maszyna].insert(Sekwencja[ktora_maszyna].begin() + nj, zadanie[0]);
			PK[zadanie[0]-1] = npk;
			NK[zadanie[0]-1] = nnk;
			
			liczba_poprzednikow(PT, PK, LP);
			KT[LiczbaOperacji-1]=0;
			kolejnosc_topologiczna(LP, PT, PK, KT);
			licz_R(R, PT, PK, KT, OperacjeNaturalna);
			licz_Q(Q, NT, NK, KT, OperacjeNaturalna);
		}
		
		
		
	}

/*	for(int i=0; i<LiczbaOperacji; i++)
	{
			if(i%LiczbaMaszyn == 0 && i!=0) cout << endl;
			cout << Q[i] << "\t";
	}*/
	for(unsigned i=0; i<LiczbaOperacji; i++)
	{
		unsigned ktora=i/LiczbaMaszyn;
		unsigned ktory=i%LiczbaMaszyn;
		SekwencjaWynikowa->push_back(Sekwencja[ktora][ktory]);
	}
	//cout << endl << Sekwencja[0][0] << endl;

	return 0;
}
















