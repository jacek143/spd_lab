/**
* @file main.cpp
*
* @brief Plik z funkcj� main().
*/

#include "schrage.h"
#include <iostream>
#include "carlier.h"
#include <fstream>

using namespace std;

/**
* @brief Funkcja main() ywo�uj�ca algorytm Carliera.
*/
int main(int argc, char** argv) {
	
	bool SchragePoprawny = SprawdzSchrage("schr.data.txt");
	if(not SchragePoprawny){
		cout << "Schrage nie dzia�a :(" << endl;
	}
	else{	
		fstream plik;
		plik.open("carl.data.txt");
		unsigned R[100], P[100], Q[100], PI[100];
		unsigned n;
		string smietnik;
		
		for(unsigned j = 0; j < 9; j++){
			plik >> smietnik;
			cout << smietnik << endl;
			plik >> n;
			for(unsigned i = 0; i < n; i++){
				plik >> R[i] >> P[i] >> Q[i];
			} // for
			cout << Carlier2(n, R, P, Q, PI) << endl;
			for(unsigned i = 0; i < n; i++){
				cout << PI[i] << " ";
			} // for
			cout << endl;
			
			for(unsigned i = 0; i < (n+2); i++){
				plik >> smietnik;
			} // for
		} // for
		
		plik.close();
	}
	return 0;
}
