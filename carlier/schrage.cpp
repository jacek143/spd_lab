/**
* @file schrage.cpp
*
* @brief Kod �r�d�owy algorytm�w Schrage.
*/

#include "schrage.h"
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;

unsigned Schrage(std::list<Zadanie_t> ListaZadan, WariantSchrage_t Wariant, std::list<Zadanie_t> * pPermutacjaWynikowa){
	unsigned retval = 0;
	unsigned t = 0; // obecny czas
	list<Zadanie_t> ListaGotowych; // lista zadan gotowych do przetwarzania
		
	if(ListaZadan.empty()){
		return 0;
	}
	
	if(pPermutacjaWynikowa != nullptr){
		pPermutacjaWynikowa->clear();
	}
	
	for(list<Zadanie_t>::iterator it = ListaZadan.begin(); it != ListaZadan.end(); it++){
		it->zeruj_obecne_p();
	} // for
	
	ListaZadan.sort(Zadanie_t::czy_r1_mniejsze);
	t = 0;
	while(ListaZadan.size() or ListaGotowych.size()){
		if(ListaZadan.size()){
			while(ListaZadan.begin()->zwroc_r() <= t){
				ListaGotowych.push_back(*(ListaZadan.begin()));
				ListaZadan.pop_front();
				if(ListaZadan.empty()){
					break;
				} // if
			} // while
		} // if
		if(ListaGotowych.size()){
			ListaGotowych.sort(Zadanie_t::czy_q1_wieksze);
			while(ListaGotowych.begin()->zwroc_p() == ListaGotowych.begin()->zwroc_obecne_p()){
				unsigned potencjalny_nowy_cmax = ListaGotowych.begin()->zwroc_q() + t; 
				if(retval < potencjalny_nowy_cmax){
					retval = potencjalny_nowy_cmax;
				}
				ListaGotowych.pop_front();
				if(ListaGotowych.empty()){
					break;
				} // if
			} // while
			if(ListaGotowych.size()){
				if(Wariant == ZPodzialem){
					ListaGotowych.begin()->wykonaj(1);	
				}
				else{
					ListaGotowych.begin()->wykonaj(ListaGotowych.begin()->zwroc_p());
					t += ListaGotowych.begin()->zwroc_p() - 1;
					if(pPermutacjaWynikowa != nullptr){
						pPermutacjaWynikowa->push_back(*(ListaGotowych.begin()));
					}
				}
			} // if
		} // if
		t++;
	} // while
	

	
	return retval;
}

bool SprawdzSchrage(std::string NazwaPliku){
	string Smietnik; // tu dajemy niepotrzebne dane z pliku wej 
	fstream PlikWej;
	unsigned n; // liczba zada�
	unsigned r, p, q;
	list<Zadanie_t> ListaZadan;
	unsigned Schr, SchrPmtn;
	bool SchragePoprawny = true;
	
	PlikWej.open(NazwaPliku.c_str());
	if(PlikWej.fail()){
		cerr << "Blad otwarcia pliku wejsciowego." << endl;
		PlikWej.close();
		return false;
	}

	for(unsigned i = 0; i < 9; i++){
		ListaZadan.clear();
		PlikWej >> Smietnik;
		PlikWej >> n;
		for(unsigned i = 0; i < n; i++){
			PlikWej >> r >> p >> q;
			ListaZadan.push_back(Zadanie_t(i+1, r, p, q));
		} // for
		PlikWej >> Smietnik;
		PlikWej >> SchrPmtn;
		PlikWej >> Smietnik;
		PlikWej >> Schr;
		list<Zadanie_t> PermutacjaWynikowa;
		list<unsigned> uPrawidlowaPermutacja;
		for(unsigned i = 0; i < n; i++){
			unsigned ID;
			PlikWej >> ID;
			uPrawidlowaPermutacja.push_back(ID);
		} // for
		if(Schrage(ListaZadan, BezPodzialu, &PermutacjaWynikowa) != Schr){
			cerr << "Blad przy Schrage bez podzialu!" << endl;
			SchragePoprawny = false;
		} // if
		else if(Schrage(ListaZadan, ZPodzialem)	!= SchrPmtn){
			cerr << "Blad przy Schrage z podzialem!" << endl;
			SchragePoprawny = false;
		} // else if
		else {
			list<unsigned>::iterator uit = uPrawidlowaPermutacja.begin();
			list<Zadanie_t>::iterator zit = PermutacjaWynikowa.begin();
			for(; uit != uPrawidlowaPermutacja.end() and zit != PermutacjaWynikowa.end(); zit++, uit++){
				if((*uit) != zit->zwroc_id()){
					cerr << "Nieprawidlowa permutacja!" << endl;
					SchragePoprawny = false;
				} // if			
			} // for
			if(uit != uPrawidlowaPermutacja.end() or zit != PermutacjaWynikowa.end()){
				cout << "Nieprawidlowa dlugosc permutacji!" << endl;
				SchragePoprawny = false;
			}
		} // else
	} // for
	PlikWej.close();	
	
	return SchragePoprawny;
}
