/**
* @file zadanie.h
*
* @brief Klasa reprezentuj�ca zadanie.
*/

#ifndef ZADANIE_H
#define ZADANIE_H

#include <list>

/**
* @brief Reprezentacja zadania.
*/
class Zadanie_t{
	public:
		/**
		* @brief Zmienia czas dostarczenia.
		*
		* @param q - Nowy czas dostarczenia.
		*/
		void zmien_q(unsigned q);
		/**
		* @brief Zmienia czas przygotowania.
		* 
		* @param r - Nowy czas przygotowania.
		*/
		void zmien_r(unsigned r);
		/**
		* @brief Przetwarza zadanie przez podany czas.
		*
		* @param t - Czas przetwarzania.
		*/ 
		void wykonaj(unsigned t);
		/**
		* @brief Konstruktor.
		*
		* @param id - Numer ID zadania.
		*
		* @param r - Czas przygotowania.
		*
		* @param p - Czas wykonywania.
		*
		* @param q - Czas dostarczenia.
		*/
		Zadanie_t(const unsigned id, const unsigned r, const unsigned p, const unsigned q);
		/**
		* @brief Zwraca nr ID.
		*/
		unsigned zwroc_id(void) const;
		/**
		* @breif Zwraca czas przygotowywania.
		*/
		unsigned zwroc_r(void) const;
		/**
		* @brief Zwraca czas wykonywania.
		*/
		unsigned zwroc_p(void) const;
		/**
		* @brief Zwraca czas dostarczenia.
		*/
		unsigned zwroc_q(void) const;
		/**
		* @brief Zwraca dotychczasowy czas wykonywania.
		*/
		unsigned zwroc_obecne_p(void) const;
		/**
		* @brief Zeruje dotychczasowy czas wykonywania.
		*/
		void zeruj_obecne_p(void);
		/**
		* @brief Czy r pierwszego argumentu jest mniejsze ni� drugiego.
		*
		*/
		static bool czy_r1_mniejsze(const Zadanie_t arg1, const Zadanie_t arg2);
		/**
		* @brief Czy q pierwszego argumentu jest wi�ksze ni� drugiego.
		*
		*/
		static bool czy_q1_wieksze(const Zadanie_t arg1, const Zadanie_t arg2);
	private:
		/**
		* @brief Numer ID zadania.
		*
		* Zadania numerujemy od 1, a nie od 0.
		*/
		unsigned _id;
		/**
		* @brief Czas przygotowania.
		*/
		unsigned _r;
		/*
		* @brief Czas wykonywania.
		*/
		unsigned _p;
		/**
		* @brief Czas dostarczenia.
		*/
		unsigned _q;
		/**
		* @brief Obecny czas przetworzenia.
		*
		* Czas przez jaki dane zadanie by�o ju� przetwarzane.
		*/
		unsigned _obecne_p;
};

/**
* @brief Ca�kowity czas zako�czenia dla podanego wycinka.
*
* @param Begin - Pocz�tek badanego wycinka.
*
* @param End - Koniec badanego wycinka.
*
* @retval Czas zako�czenia dla podanego wycinka permutacji.
*/
unsigned ObliczCmax(std::list<Zadanie_t>::iterator Begin, std::list<Zadanie_t>::iterator End);

#endif
