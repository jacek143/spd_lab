/**
* @file zadanie.cpp
*
* @brief Metody klasy reprezentującej zadania.
*/

#include "zadanie.h"

using namespace std;

bool Zadanie_t::czy_q1_wieksze(const Zadanie_t arg1, const Zadanie_t arg2){
	return arg1.zwroc_q() > arg2.zwroc_q();
}

void Zadanie_t::wykonaj(unsigned t){
	_obecne_p += t;
	_obecne_p = (_obecne_p > _p) ? _p : _obecne_p;
}

void Zadanie_t::zeruj_obecne_p(void){
	_obecne_p = 0;
}

Zadanie_t::Zadanie_t(const unsigned id, const unsigned r, const unsigned p, const unsigned q) : _id(id), _r(r), _p(p), _q(q), _obecne_p(0){
	;
}

unsigned Zadanie_t::zwroc_r(void) const{
	return _r;
}

unsigned Zadanie_t::zwroc_p(void) const{
	return _p;
}

unsigned Zadanie_t::zwroc_q(void) const{
	return _q;
}

unsigned Zadanie_t::zwroc_obecne_p(void) const{
	return _obecne_p;
}

unsigned Zadanie_t::zwroc_id(void) const{
	return _id;
}

bool Zadanie_t::czy_r1_mniejsze(const Zadanie_t arg1, const Zadanie_t arg2){
	return arg1.zwroc_r() < arg2.zwroc_r();
}

unsigned ObliczCmax(std::list<Zadanie_t>::iterator Begin, std::list<Zadanie_t>::iterator End){
	unsigned Cmax = 0;
	unsigned t = 0;
	for(list<Zadanie_t>::iterator it = Begin; it != End; it++){
		if(it->zwroc_r() > t){
			t = it->zwroc_r();
		}
		t += it->zwroc_p();
		if(Cmax < (t + it->zwroc_q())){
			Cmax = t + it->zwroc_q();
		}
	} // for
	return Cmax;
}

void Zadanie_t::zmien_q(unsigned q){
	_q = q;
}

void Zadanie_t::zmien_r(unsigned r){
	_r = r;
}
