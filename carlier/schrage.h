/**
* @file schrage.h
*
* @brief Funkcje implementuj�ce algorytm Schrage z przerywaniem i bez.
*/

#ifndef SCHRAGE_H
#define SCHRAGE_H

#include <list>
#include "zadanie.h"
#include <cstddef>
#include <string>

/**
* @brief Warianty algorytmu Schrage.
*/
enum WariantSchrage_t {
	/**
	* @brief Algorytm Schrage z podzia�em (z przerwaniami).
	*/
	ZPodzialem,
	/**
	* @brief Algorytm Schrage bez podzia�u (bez przerwa�).
	*/
	BezPodzialu
};

/**
* @brief Algorytm Schrage z podzia�em.
*
* @param ListaZadan - Nieuporz�dkowana lista zada�.
*
* @param Wariant - Wariant dzia�ania algorytmu Schrage: z podzia�em zada� lub bez.
*
* @param pPermutacjaWynikowa - Wska�nik na miejsce, w kt�rym ma zosta� 
* zapisana wynikowa permutacja dla algorytmu Schrage z podzia�em.
*
* @retval Czas potrzebny uko�czenie wszystkich zada�.
*/
unsigned Schrage(std::list<Zadanie_t> ListaZadan, WariantSchrage_t Wariant, std::list<Zadanie_t> * pPermutacjaWynikowa = nullptr);

/**
* @brief Sprawdza, czy algorytm Schrage z podazia�em i bez dzia�a poprawnie.
* 
* @param NazwaPliku - Nazwa pliku z danymi dla Schrage.
*
* @retval Czy algorytm dzia�a poprawnie.
*/
bool SprawdzSchrage(std::string NazwaPliku);

#endif
