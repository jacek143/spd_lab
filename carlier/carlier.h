/**
* @file carlier.h
*
* @brief Deklaracje funkcji implementuj�cych algorytm Carliera.
*/

#ifndef CARLIER_H
#define CARLIER_H

#include <list>
#include <string>
#include "zadanie.h"

/**
* @brief Algorytm Carliera.
*
* @param ListaZadan - Lista zada�, kt�re maj� by� uszeregowane.
*
* @param pPermutacjaWynikowa - Wska�nik na miejsce, gdzie ma zosta� zapisana permutacja znaleziona przez algorytm.
*
* @param CzyWywolanieRekurencyjne - Czy funkcja jest wywo�ywana z tej samej funkcji (rekurencyjnie).
*
* @retval Czas jaki b�dzie potrzebny na zako�czenie znalezionej permutacji.
*/
unsigned Carlier(std::list<Zadanie_t> ListaZadan, std::list<Zadanie_t> * pPermutacjaWynikowa = nullptr, bool CzyWywolanieRekurencyjne = false);

/**
* @brief Sprawdza, czy algorytm Carliera dzia�a poprawnie.
* 
* @param NazwaPliku - Nazwa pliku z danymi dla Schrage.
*
* @retval Czy algorytm dzia�a poprawnie.
*/
bool SprawdzCarlier(std::string NazwaPliku);

unsigned Carlier2(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PIprim[]);

#endif /* CARLIER_H */
