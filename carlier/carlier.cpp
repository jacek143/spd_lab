/**
* @file carlier.cpp
*
* @brief Definicje funkcji implementujących algorytm Carliera.
*/

#include "carlier.h"
#include "schrage.h"
#include <string>
#include <fstream>
#include <iostream>
#include <climits>

using namespace std;

unsigned Carlier(std::list<Zadanie_t> ListaZadan, std::list<Zadanie_t> * pPermutacjaWynikowa, bool CzyWywolanieRekurencyjne){
	
	static unsigned ub;
	list<Zadanie_t> Optymalna = *pPermutacjaWynikowa;
	
	if(not CzyWywolanieRekurencyjne){
		ub = UINT_MAX;
	}

	list<Zadanie_t> PermutacjaWynikowa;
	unsigned u = Schrage(ListaZadan, BezPodzialu, &PermutacjaWynikowa);
	if(u < ub){
		ub = u;
		Optymalna = PermutacjaWynikowa;
		ListaZadan = PermutacjaWynikowa;
	}
	
	
	unsigned a = 0, b = 0, c = 0;
	list<Zadanie_t>::iterator ita, itb, itc;

	unsigned Cmax = ObliczCmax(ListaZadan.begin(), ListaZadan.end());	
	list<Zadanie_t> tmpList = ListaZadan;
	for(unsigned i = 0; i < ListaZadan.size(); i++){
		tmpList.pop_front();
		if(Cmax != ObliczCmax(tmpList.begin(), tmpList.end())){
			a = i;
			break;
		} // if
	} // for
	
	tmpList = ListaZadan;
	for(unsigned i = 0; i < ListaZadan.size(); i++){
		tmpList.pop_back();
		if(Cmax != ObliczCmax(tmpList.begin(), tmpList.end())){
			b = ListaZadan.size() - i - 1;
			break;
		} // if		
	} // for
	
	{
		unsigned i = 0;
		list<Zadanie_t>::iterator it = ListaZadan.begin();
		while(i < ListaZadan.size()){
			if(i == a){
				ita = it;
			}
			if(i == b){
				itb = it;
				break;
			}
			it++;
			i++;
		}
	}

	bool c_istnieje = false;	
	{
		list<Zadanie_t>::iterator it;
		unsigned i;
		for(i = a, it = ita; i < b; i++, it++){
			if(it->zwroc_q() < itb->zwroc_q()){
				c_istnieje = true;
				c = i;
				itc = it;				
			}
		} // for		
	} 
	
	if(c_istnieje){
		unsigned qPrim = itb->zwroc_q();
		unsigned pPrim = 0;
		for(list<Zadanie_t>::iterator it = ita; it != itb; it++){
			pPrim += it->zwroc_p();
		}
		pPrim += itb->zwroc_p();
		unsigned rPrim;
		for(list<Zadanie_t>::iterator it = ita; it != itb; it++){
			if(it->zwroc_r() < rPrim){
				rPrim = it->zwroc_r();
			}
		}
		if(itb->zwroc_r() < rPrim){
			rPrim = itb->zwroc_r();
		}
						
		unsigned StareRc = itc->zwroc_r();
		unsigned IDc = itc->zwroc_id();
		if(itc->zwroc_r() < (rPrim + pPrim)){
			itc->zmien_r(rPrim + pPrim);
		} // if
		unsigned lb = Schrage(ListaZadan, ZPodzialem);
		if(lb < ub){
			Carlier(ListaZadan, &Optymalna, true);
		} // if
		itc->zmien_r(StareRc);
		for(list<Zadanie_t>::iterator it = Optymalna.begin(); it != Optymalna.end(); it++){
			if(IDc == it->zwroc_id()){
				it->zmien_r(StareRc);
			} // if
		} // for
		
		unsigned StareQc = itc->zwroc_q();
		if(itc->zwroc_q() < (qPrim + pPrim)){
			itc->zmien_q(qPrim + pPrim);
		} // if
		lb = Schrage(ListaZadan, ZPodzialem);
		if(lb < ub){
			Carlier(ListaZadan, &Optymalna, true);
		} // if
		itc->zmien_q(StareQc);
		for(list<Zadanie_t>::iterator it = Optymalna.begin(); it != Optymalna.end(); it++){
			if(IDc == it->zwroc_id()){
				it->zmien_q(StareQc);
			} // if
		} // for
	}
	
	if(pPermutacjaWynikowa != nullptr){
		*pPermutacjaWynikowa = Optymalna;
	}
		
	return ObliczCmax(Optymalna.begin(), Optymalna.end());
} 

bool SprawdzCarlier(std::string NazwaPliku){
	string Smietnik; // tu dajemy niepotrzebne dane z pliku wej 
	fstream PlikWej;
	unsigned n; // liczba zadań
	unsigned r, p, q;
	list<Zadanie_t> ListaZadan;
	unsigned PrawidlowyWycnik;
	bool Poprawny = true;
	
	PlikWej.open(NazwaPliku.c_str());
	if(PlikWej.fail()){
		cerr << "Blad otwarcia pliku wejsciowego." << endl;
		PlikWej.close();
		return false;
	}


	for(unsigned i = 0; i < 1; i++){
		ListaZadan.clear();
		PlikWej >> Smietnik;
		PlikWej >> n;
		for(unsigned i = 0; i < n; i++){
			PlikWej >> r >> p >> q;
			ListaZadan.push_back(Zadanie_t(i, r, p, q));
		} // for
		PlikWej >> Smietnik;
		PlikWej >> PrawidlowyWycnik;
		list<Zadanie_t> PermutacjaWynikowa;
		list<unsigned> uPrawidlowaPermutacja;
		for(unsigned i = 0; i < n; i++){
			unsigned ID;
			PlikWej >> ID;
			uPrawidlowaPermutacja.push_back(ID);
		} // for
		if(Carlier(ListaZadan, &PermutacjaWynikowa) != PrawidlowyWycnik){
			cerr << "Zly czas zakonczenia wszystkich dzialan" << endl;
			Poprawny = false;
		} // if
		else {
			list<unsigned>::iterator uit = uPrawidlowaPermutacja.begin();
			list<Zadanie_t>::iterator zit = PermutacjaWynikowa.begin();
			for(; uit != uPrawidlowaPermutacja.end() and zit != PermutacjaWynikowa.end(); zit++, uit++){
				if((*uit) != zit->zwroc_id()){
					cerr << "Nieprawidlowa permutacja!" << endl;
					Poprawny = false;
				} // if			
			} // for
			if(uit != uPrawidlowaPermutacja.end() or zit != PermutacjaWynikowa.end()){
				cout << "Nieprawidlowa dlugosc permutacji!" << endl;
				Poprawny = false;
			}
		} // else
	} // for
	PlikWej.close();	
	
	return Poprawny;
}

unsigned ObliczCmax(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PI[]);

// zakladamy numeracje zadan od zera!!!!!!!!!!
unsigned Schrage2(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PI[]){
		list<Zadanie_t> ListaZadan;
		list<Zadanie_t> Wynikowa;
		for(unsigned i = 0; i < n; i++){
			ListaZadan.push_back(Zadanie_t(i, R[i], P[i], Q[i]));
		} // for

		unsigned U = Schrage(ListaZadan, BezPodzialu, &Wynikowa);
		
		unsigned i = 0;
		for(list<Zadanie_t>::iterator it = Wynikowa.begin(); it != Wynikowa.end(); it++){
			PI[i++] = it->zwroc_id();
		} // for
		
		return U;
}

// zakladamy numeracje zadan od zera!!!!!!!!!!
unsigned SchragePmtn2(unsigned n, unsigned R[], unsigned P[], unsigned Q[]){
	list<Zadanie_t> ListaZadan;
	for(unsigned i = 0; i < n; i++){
		ListaZadan.push_back(Zadanie_t(i, R[i], P[i], Q[i]));
	} // for
	return Schrage(ListaZadan, ZPodzialem);
}

unsigned ObliczC(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PI[]);

unsigned Carlier2(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PIprim[]){
	static unsigned StRekurencji = 0;
	static unsigned UB;
	static unsigned PI[100];
	
	if(StRekurencji == 0){
		UB = UINT_MAX;
		for(unsigned i = 0; i < n; i++){
			PI[i] = i;
			PIprim[i] = i;
		} // for
	} // if
	StRekurencji++;
	
	unsigned U = Schrage2(n, R, P, Q, PI);
	if(U < UB){
		UB = U;
		for(unsigned i = 0; i < n; i++){
			PIprim[i] = PI[i];
		} // for
	} // if
	
	unsigned b = 0;
	for(unsigned i = 0; i < n; i++){
		if((ObliczC(i+1, R, P, Q, PI) + Q[PI[i]]) == U){
			b = i;
		} // if
	} // for

	unsigned a = 0;
	for(unsigned i = 0; i < n; i++){
		unsigned SumaP = 0;
		for(unsigned k = i; k < (b+1); k++){
			SumaP += P[PI[k]];
		} // for
		if(U == (R[PI[i]] + SumaP + Q[PI[b]])){
			a = i;
			break;
		} // if
	} // for

	unsigned c = 0;
	bool cIstnieje = false;
	for(unsigned i = a; i < b; i++){
		if(Q[PI[i]] < Q[PI[b]]){
			cIstnieje = true;
			c = i;
		}
	} // for
	
	if(cIstnieje){
		unsigned rprim = UINT_MAX;
		unsigned qprim = UINT_MAX;
		unsigned pprim = 0;
		for(unsigned i = c+1; i < (b+1); i++){
			pprim += P[PI[i]];
			if(rprim > R[PI[i]]){
				rprim = R[PI[i]];
			} // if
			if(qprim > Q[PI[i]]){
				qprim = Q[PI[i]];
			} // if
		} // for
		
		unsigned rcStare = R[PI[c]];
		if((rprim + pprim) > R[PI[c]]){
			R[PI[c]] = rprim + pprim;
		} // if
		unsigned LB = SchragePmtn2(n, R, P, Q);
		if(LB < UB){
			Carlier2(n, R, P, Q, PIprim);
		}
		R[PI[c]] = rcStare;
		
		unsigned qcStare = Q[PI[c]];
		if((qprim + pprim) > Q[PI[c]]){
			Q[PI[c]] = qprim + pprim;
		} // if
		LB = SchragePmtn2(n, R, P, Q);
		if(LB < UB){
			Carlier2(n, R, P, Q, PIprim);
		}
		Q[PI[c]] = qcStare;
		
	} // if
	
	StRekurencji--;
	return ObliczCmax(n, R, P, Q, PIprim);
}

unsigned ObliczCmax(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PI[]){
	// numerujemy od zera !!!
	list<Zadanie_t> Lista;
	for(unsigned i = 0; i < n; i++){
		Lista.push_back(Zadanie_t(PI[i], R[PI[i]], P[PI[i]], Q[PI[i]]));
	} // for
	return ObliczCmax(Lista.begin(), Lista.end());	
}

unsigned ObliczC(unsigned n, unsigned R[], unsigned P[], unsigned Q[], unsigned PI[]){
	unsigned retval = 0;
	for(unsigned i = 0; i < n; i++){
		if(retval < R[PI[i]]){
			retval = R[PI[i]];
		}
		retval += P[PI[i]];
	} // for
	return retval;
}
