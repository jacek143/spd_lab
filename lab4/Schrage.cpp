#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <vector>

#define NAZWA "schr.data.txt"


using namespace std;

unsigned n;

class zadanie{
	public:
		unsigned ret_r(){ return _r; }
		unsigned ret_p(){ return _p; }
		unsigned ret_q(){ return _q; }
		unsigned ret_l(){ return _l; }
		void set_r(unsigned r){ _r=r; }
		void set_p(unsigned p){ _p=p; }
		void set_q(unsigned q){ _q=q; }
		void set_l(unsigned l){ _l=l; }
	
	private:
		unsigned _r, _p, _q, _l;
};

bool compare1 (zadanie zad1, zadanie zad2)
{
  if(zad1.ret_r() < zad2.ret_r()) return true;
  if(zad1.ret_r() > zad2.ret_r()) return false;
  return false;
}

bool compare2 (zadanie zad1, zadanie zad2)
{
  if(zad1.ret_q() > zad2.ret_q()) return true;
  if(zad1.ret_q() < zad2.ret_q()) return false;
  return false;
}

int main()
{
	fstream plik(NAZWA);
	for(unsigned it=0; it<9; it++){
	string smietnik;
	string SchrPmtn;
	string Schr;
	unsigned SchrPmtn_cmax, Schr_cmax, sekwencja;
	vector<zadanie> N;
	vector<zadanie> G;
	vector<zadanie> wynikowa;
	zadanie zad;
	unsigned r, p, q;
	
	plik >> smietnik;
	cout << smietnik << endl;
	plik >> n;
	for(unsigned i=0; i<n; i++){
	plik >> r >> p >> q;
	zad.set_p(p);
	zad.set_q(q);
	zad.set_r(r);
	zad.set_l(i+1);
	N.push_back(zad);
	}
	plik >> SchrPmtn >> SchrPmtn_cmax;
	plik >> Schr >> Schr_cmax;
	cout << SchrPmtn << SchrPmtn_cmax << endl;
	//cout << Schr << Schr_cmax << endl;
	for(unsigned i=0; i<n; i++){
	plik >> sekwencja;
	//cout << sekwencja << " ";
	}
	
	sort(N.begin(), N.end(), compare1);
	
	unsigned t=0, cmax=0;
	zadanie e, k;
	k.set_q(0);
	while(!N.empty() || !G.empty()){
		while(!N.empty() && N.begin()->ret_r()<=t){
			e=N.front();
			G.push_back(e);
			N.erase(N.begin());
			if(e.ret_q()>k.ret_q()){
				k.set_p(t-e.ret_r());
				t=e.ret_r();
				if(k.ret_p()>0) G.push_back(k); 
			}
		} // while2
		if(G.empty()) t=N.begin()->ret_r();
		else{
			sort(G.begin(), G.end(), compare2);
			e=G.front();
			G.erase(G.begin());
			//wynikowa.push_back(e);
			k=e;
			t=t+e.ret_p();
			cmax=max(cmax,t+e.ret_q());
		} // else
	} // while1
	
	//cout << endl << "Permutacja obliczona: ";
		/*for(vector<zadanie>::iterator it=wynikowa.begin();it<wynikowa.end();it++)
	{
		cout << it->ret_l() << " ";
	}*/
	cout << endl << "Obliczony cmax: " << cmax << endl << endl;	
	} // g��wna p�tla
	return 0;
}
