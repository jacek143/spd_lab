#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include "schrage.h"

using namespace std;

bool compare1 (zadanie zad1, zadanie zad2)
{
  if(zad1.ret_r() < zad2.ret_r()) return true;
  if(zad1.ret_r() > zad2.ret_r()) return false;
  return false;
}

bool compare2 (zadanie zad1, zadanie zad2)
{
  if(zad1.ret_q() > zad2.ret_q()) return true;
  if(zad1.ret_q() < zad2.ret_q()) return false;
  return false;
}

unsigned SchrPmtn(vector<zadanie> N)
{
	vector<zadanie> G;
	vector<zadanie> wynikowa;
	zadanie zad;
	
	sort(N.begin(), N.end(), compare1);
	
	unsigned t=0, cmax=0;
	zadanie e, k;
	k.set_q(0);
	while(!N.empty() || !G.empty()){
		while(!N.empty() && N.begin()->ret_r()<=t){
			e=N.front();
			G.push_back(e);
			N.erase(N.begin());
			if(e.ret_q()>k.ret_q()){
				k.set_p(t-e.ret_r());
				t=e.ret_r();
				if(k.ret_p()>0) G.push_back(k); 
			}
		} // while2
		if(G.empty()) t=N.begin()->ret_r();
		else{
			sort(G.begin(), G.end(), compare2);
			e=G.front();
			G.erase(G.begin());
			//wynikowa.push_back(e);
			k=e;
			t=t+e.ret_p();
			cmax=max(cmax,t+e.ret_q());
		} // else
	} // while1
	
	//cout << endl << "Permutacja obliczona: ";
		/*for(vector<zadanie>::iterator it=wynikowa.begin();it<wynikowa.end();it++)
	{
		cout << it->ret_l() << " ";
	}*/

	return cmax;
}

Wynik Schr(vector<zadanie> N)
{
	vector<zadanie> G;
	vector<zadanie> wynikowa;
	zadanie zad;
	
	sort(N.begin(), N.end(), compare1);
	
	unsigned t=0, cmax=0;
	zadanie e;
	while(!N.empty() || !G.empty()){
		while(!N.empty() && N.begin()->ret_r()<=t){
			e=N.front();
			G.push_back(e);
			N.erase(N.begin());
		} // while2
		if(G.empty()) t=N.begin()->ret_r();
		else{
			sort(G.begin(), G.end(), compare2);
			e=G.front();
			G.erase(G.begin());
			wynikowa.push_back(e);
			t=t+e.ret_p();
			cmax=max(cmax,t+e.ret_q());
		} // else
	} // while1
	
	//cout << endl << "Permutacja obliczona: ";
		/*for(vector<zadanie>::iterator it=wynikowa.begin();it<wynikowa.end();it++)
	{
		cout << it->ret_l() << " ";
	}*/

	WynikSchrage retval = {cmax, wynikowa};

	return retval;
}
