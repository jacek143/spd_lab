#ifndef SCHRAGE_H
#define SCHRAGE_H

#include <vector>

class zadanie{
	public:
		unsigned ret_r(){ return _r; }
		unsigned ret_p(){ return _p; }
		unsigned ret_q(){ return _q; }
		unsigned ret_l(){ return _l; }
		void set_r(unsigned r){ _r=r; }
		void set_p(unsigned p){ _p=p; }
		void set_q(unsigned q){ _q=q; }
		void set_l(unsigned l){ _l=l; }
	
	private:
		unsigned _r, _p, _q, _l;
};

unsigned SchrPmtn(std::vector<zadanie> N);

struct Wynik {
	unsigned cmax;
	std::vector<zadanie> permutacja; 
};

Wynik Schr(std::vector<zadanie> N);

#endif 
