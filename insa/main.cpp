#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <list>

using namespace std;

// Ka�de zadanie sk�ada si� z operacji. 
// Operacje sk�adowe danego zadania musz� zosta� wykonane w �ci�le okre�lonej kolejno�ci, aby zadanie by�o wykonane poprawnie.
// Ka�da operacja wykonuje si� na jednej maszynie przez okre�lony czas.
// Operacje numerujemy od 1
struct operacja_t{
	operacja_t(unsigned NrZadania, unsigned NrWlasny, unsigned NrMaszyny, unsigned Czas, unsigned NrPoprzednika) : _NrZadania(NrZadania), _NrWlasny(NrWlasny), _NrMaszyny(NrMaszyny), _Czas(Czas), _NrPoprzednika(NrPoprzednika) {
		;
	}
	static bool porownaj_czasy(operacja_t Op1, operacja_t Op2);
	unsigned _NrZadania; // numerujemy od 1.
	unsigned _NrWlasny; // Okre�la numer operacji. Jest jedna numeracja dla wszystkich zadan. Numerujemy od 1.
	unsigned _NrMaszyny;
	unsigned _Czas;
	unsigned _NrPoprzednika; // 0 oznacza, ze nie ma poprzednika technologicznego
};

bool operacja_t::porownaj_czasy(operacja_t Op1, operacja_t Op2){
	return Op1._Czas > Op2._Czas;
}	

unsigned oblicz_c_max(vector<list<operacja_t> > Maszyny){	
	unsigned Retval = 0;
	
	unsigned LiczbaOperacji = 0;
	for(unsigned i = 0; i != Maszyny.size(); i++){
		LiczbaOperacji += Maszyny[i].size();
	}
	if(LiczbaOperacji == 0){
		return 0;
	}
	
	list<unsigned> Wykonane(1,0);
	vector<bool> MaszynyZajete(Maszyny.size(), false);
	vector<unsigned> MaszynyPrzetwarzanie(Maszyny.size(), 0);
	
	for(Retval = 0; Wykonane.size() < (LiczbaOperacji+1); Retval++){
		for(unsigned i = 0; i < Maszyny.size(); i++){
			if(MaszynyZajete[i] == false and Maszyny[i].size()){			
				bool CzyPoprzednikWykonany = true;
				operacja_t Operacja = *(Maszyny[i].begin());
				unsigned LiczbaPoprzTech = (Operacja._NrWlasny-1) % Maszyny.size();
				for(unsigned j = 0; (j < LiczbaPoprzTech) and CzyPoprzednikWykonany; j++){
				  	unsigned NrPoprzednika = Operacja._NrWlasny-j-1;
				  	for(unsigned k = 0; k < Maszyny.size() and CzyPoprzednikWykonany; k++){
				  		for(list<operacja_t>::iterator it = Maszyny[k].begin(); CzyPoprzednikWykonany and it != Maszyny[k].end(); it++){
				  			if(it->_NrWlasny == NrPoprzednika){
				  				CzyPoprzednikWykonany = false;
							}	
						} // for	
					} // for
				} // for
				if(CzyPoprzednikWykonany){
					MaszynyZajete[i] = true;
					MaszynyPrzetwarzanie[i] = 0;
				}
			} // if
		} // for
		
		bool CzyCykl = true;
		for(unsigned i = 0; i < Maszyny.size(); i++){
			if(MaszynyZajete[i] == true){
				CzyCykl = false;
			} // if
		}
		if(CzyCykl){
			return UINT_MAX;
		}
		
		for(unsigned i = 0; i < Maszyny.size(); i++){
			if(MaszynyZajete[i] == true){
				MaszynyPrzetwarzanie[i]++;
				if(MaszynyPrzetwarzanie[i] == Maszyny[i].begin()->_Czas){
					MaszynyZajete[i] = false;
					Wykonane.push_back(Maszyny[i].begin()->_NrWlasny);
					Maszyny[i].erase(Maszyny[i].begin());
				} // if
			} // if
		}
	} // for
		
	return Retval;
}

unsigned insa(unsigned LiczbaMaszyn, vector<operacja_t> Operacje, vector<unsigned> * pSekwencjaWynikowa){
	vector<list<operacja_t> > Maszyny(LiczbaMaszyn, list<operacja_t>());
	
	pSekwencjaWynikowa->clear();
	sort(Operacje.begin(), Operacje.end(), operacja_t::porownaj_czasy);
	
	for(unsigned i = 0; i != Operacje.size(); i++){
		operacja_t DodawanaOperacja = Operacje[i];
		unsigned IndeksMaszyny = DodawanaOperacja._NrMaszyny-1;
		unsigned IloscPodstawien = Maszyny[IndeksMaszyny].size()+1;
		
		Maszyny[IndeksMaszyny].push_back(DodawanaOperacja);
		unsigned NajCMax = oblicz_c_max(Maszyny);
		list<operacja_t> NajlepszeUszeregowanie = Maszyny[IndeksMaszyny];
		Maszyny[IndeksMaszyny].pop_back();
	
		list<operacja_t> KopiaMaszyny = Maszyny[IndeksMaszyny];
		for(unsigned j = 0; j < KopiaMaszyny.size(); j++){
			Maszyny[IndeksMaszyny] = KopiaMaszyny;
			list<operacja_t>::iterator it = Maszyny[IndeksMaszyny].begin();
			for(unsigned k = 0; k < KopiaMaszyny.size(); k++){
				if(k == j){
					Maszyny[IndeksMaszyny].insert(it, DodawanaOperacja);
					unsigned CMax = oblicz_c_max(Maszyny);
					if(NajCMax > CMax){
						NajCMax = CMax;
						NajlepszeUszeregowanie = Maszyny[IndeksMaszyny];
					} // if
					break;
				} // if
				it++;
			} // for
		} // for
		Maszyny[IndeksMaszyny] = NajlepszeUszeregowanie;
	} // for
	
	/*
	Maszyny[0].push_back(Operacje[8]);
	Maszyny[0].push_back(Operacje[5]);
	Maszyny[0].push_back(Operacje[12]);
	Maszyny[0].push_back(Operacje[1]);

	Maszyny[1].push_back(Operacje[9]);
	Maszyny[1].push_back(Operacje[6]);
	Maszyny[1].push_back(Operacje[14]);
	Maszyny[1].push_back(Operacje[3]);
	
	Maszyny[2].push_back(Operacje[0]);
	Maszyny[2].push_back(Operacje[10]);
	Maszyny[2].push_back(Operacje[13]);
	Maszyny[2].push_back(Operacje[7]);
	
	Maszyny[3].push_back(Operacje[4]);
	Maszyny[3].push_back(Operacje[11]);
	Maszyny[3].push_back(Operacje[2]);
	Maszyny[3].push_back(Operacje[15]);
	*/
	
	for(unsigned i = 0; i < Maszyny.size(); i++){
		for(list<operacja_t>::iterator it = Maszyny[i].begin(); it != Maszyny[i].end(); it++){
			pSekwencjaWynikowa->push_back(it->_NrWlasny);
		} // for
	} // for
	
	return oblicz_c_max(Maszyny);
}

int main(int argc, char *argv[])
{
	const string NazwaPliku = "insa.data.txt";
	const unsigned LiczbaPrzykladow = 1;
	fstream Plik;	
	
	Plik.open(NazwaPliku.c_str());
	
	for(unsigned i = 0; i < LiczbaPrzykladow; i++){
		vector<operacja_t> Operacje;
		string Smietnik;
		unsigned LiczbaMaszyn;
		unsigned LiczbaOperacji;
		
		Plik >> Smietnik; // nazwa instancji
		Plik >> LiczbaMaszyn;
		Plik >> Smietnik; // niepotrzebne powtorzenie liczby maszyn
		Plik >> LiczbaOperacji;
		
		unsigned NrOperacji = 0, NrZadania = 0;
		for(unsigned j = 0; j < LiczbaMaszyn; j++){
			NrZadania++;
			Plik >> Smietnik; // liczba zadan na maszynie 
			for(unsigned k = 0; k < LiczbaMaszyn; k++){ // w naszym przypadku zada� na maszynie jest tyle samo co maszyn
				unsigned NrMaszyny, Czas;
				Plik >> NrMaszyny;
				Plik >> Czas;
				Operacje.push_back(operacja_t(NrZadania, ++NrOperacji, NrMaszyny, Czas, ((k == 0) ? 0 : (NrOperacji))));
			} // for
		} // for
		
		vector<unsigned> SekwencjaWynikowa;
		unsigned OtrzymanyCMax = insa(LiczbaMaszyn, Operacje, &SekwencjaWynikowa);
				
		unsigned PrawidlowyCMax;
		Plik >> Smietnik; // tekst "insa"
		Plik >> PrawidlowyCMax;
		vector<unsigned> PrawidlowaSekwencja;
		for(unsigned j = 0; j < (LiczbaMaszyn * LiczbaMaszyn); j++){
			unsigned NrOperacji;
			Plik >> NrOperacji;
			PrawidlowaSekwencja.push_back(NrOperacji);
		} // for
		
		if(OtrzymanyCMax != PrawidlowyCMax){
			cerr << "Nieprawidlowy Cmax" << endl;
		}
		
		cout << endl;
		cout << "insa:" << endl;
		cout << OtrzymanyCMax;
		for(unsigned j = 0; j < SekwencjaWynikowa.size(); j++){
			if(j % LiczbaMaszyn == 0){
				cout << endl;
			}
			cout << SekwencjaWynikowa[j] << " ";
		} // for
		cout << endl;
		
	} // for
	
	
	Plik.close();
	system("pause");
	return 0;
}
