#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>

#define NAZWA "schr.data.txt"
#define N 999

using namespace std;

unsigned n, r[N], p[N], q[N], indeksy_r[N], indeksy_q[N], wynikowa[N]; // Zmienne globalne

int compare1 (const void * a, const void * b)
{
 return ( r[(*(int*)a)-1] - r[(*(int*)b)-1] );
}

int compare2 (const void * a, const void * b)
{
 return ( q[(*(int*)b)-1] - q[(*(int*)a)-1] );
}

void funckja(unsigned ile_przyjetych, unsigned ile_zrobionych){
	unsigned pomocnicza[ile_przyjetych-ile_zrobionych];
	for(unsigned i=0; i<ile_przyjetych-ile_zrobionych; i++) pomocnicza[i]=indeksy_q[i+ile_zrobionych];
	qsort(pomocnicza, ile_przyjetych-ile_zrobionych, sizeof(unsigned), compare2);
	for(unsigned i=0; i<ile_przyjetych-ile_zrobionych; i++) indeksy_q[i+ile_zrobionych]=pomocnicza[i];
}

int main()
{
	fstream plik(NAZWA);
	for(unsigned it=0; it<9; it++){
	string smietnik;
	string SchrPmtn;
	string Schr;
	unsigned SchrPmtn_cmax, Schr_cmax, sekwencja;
	
	plik >> smietnik;
	cout << smietnik << endl;
	plik >> n;
	for(unsigned i=0; i<n; i++) plik >> r[i] >> p[i] >> q[i];
	plik >> SchrPmtn >> SchrPmtn_cmax;
	plik >> Schr >> Schr_cmax;
	cout << SchrPmtn << SchrPmtn_cmax << endl;
	for(unsigned i=0; i<n; i++){
	plik >> sekwencja;
	//cout << sekwencja << " ";
	}
	
	for(unsigned i=0; i<n+1; i++) indeksy_r[i]=i+1;
	qsort(indeksy_r, n, sizeof(unsigned), compare1);
	
	unsigned t=0, temp=0, cmax=0, ile_przyjetych=0, ile_zrobionych=0, ile_oczekujacych=n;
	while(ile_oczekujacych!=0 || ile_zrobionych!=n){
		while(ile_oczekujacych!=0 && (r[indeksy_r[ile_przyjetych]-1]<=t)){
			indeksy_q[ile_przyjetych]=indeksy_r[ile_przyjetych];
			ile_przyjetych++;
			ile_oczekujacych--;
		} // while2
		if(ile_przyjetych==ile_zrobionych) t=r[indeksy_r[ile_przyjetych]-1];
		else{
			funckja(ile_przyjetych, ile_zrobionych);
			if( q[indeksy_r[ile_przyjetych]-1]>q[indeksy_q[ile_zrobionych]-1] && r[indeksy_r[ile_przyjetych]-1]<(t+p[indeksy_q[ile_zrobionych]-1]) ) {
				do{
				indeksy_q[ile_przyjetych]=indeksy_r[ile_przyjetych];
				ile_przyjetych++;
				ile_oczekujacych--;
				} while(r[indeksy_r[ile_przyjetych-1]-1]==r[indeksy_r[ile_przyjetych]-1]);
				funckja(ile_przyjetych, ile_zrobionych);
				p[indeksy_q[ile_zrobionych+1]-1]=t+p[indeksy_q[ile_zrobionych+1]-1]-r[indeksy_q[ile_zrobionych]-1];
				t=r[indeksy_q[ile_zrobionych]-1];
			}
			else{
			//wynikowa[ile_zrobionych]=indeksy_q[ile_zrobionych];
			t+=p[indeksy_q[ile_zrobionych]-1];
			cmax=max(cmax,t+q[indeksy_q[ile_zrobionych]-1]);
			ile_zrobionych++;
			}
		} // else
	} // while1
	
	//cout << endl << "Permutacja obliczona: ";
	//for(unsigned i=0; i<n; i++) cout << wynikowa[i] << " ";
	cout << endl << "Obliczony cmax: " << cmax << endl << endl;	
	} // g��wna p�tla
	return 0;
}
