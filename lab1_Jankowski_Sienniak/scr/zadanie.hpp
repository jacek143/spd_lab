/**
 * @file zadanie.hpp
 *
 * @brief Plik zawiera klase reprezentujaca pojedyncze zadanie.
 */

#ifndef ZADANIE_HPP
#define ZADANIE_HPP

#include <iostream>

/**
 * @brief Klasa reprezentujaca pojedyncze zadanie.
 */
class Zadanie_t{

public:

  /**
   * @brief Tworzy obiekt i przypisuje mu na stale okreslone prametry.
   *
   * @param r - Czas przygotowanie.
   *
   * @param p - Czas wykonwyania zadania.
   *
   * @param q - Czas stygniecia.
   *
   * @details Wszystkie parametry sa domniemane jako 0.
   */
  Zadanie_t(unsigned int r = 0, unsigned int p = 0, unsigned int q = 0, unsigned int n = 0) : _r(r), _p(p), _q(q), _n(n){
    ;
  }

  /**
   * @brief Zwraca numer zadania.
   *
   * @retval Numer zadania.
   */
  unsigned int ret_n(void) const {
    return _n;
  }

  /**
   * @brief Zwaraca czas przygotowania.
   *
   * @retval Czas przygotowania.
   */
  unsigned int ret_r(void) const{
    return _r;
  }

  /**
   * @brief Zwraca czas wykonwyania zadania.
   *
   * @retval Czas wykonywania zadania.
   */
  unsigned int ret_p(void) const{
    return _p;
  }

  /**
   * @brief Zwaraca czas stygniecia.
   *
   * @retval Czas stydniecia.
   */
  unsigned int ret_q(void) const{
    return _q;
  }

  /**
   * @brief Zmienia numer przypisany do zadania.
   *
   * @param n - Nowy numer zadania.
   */
  void zmien_n(unsigned int n) {
    this->_n = n;
  }

  /**
   * @brief Wczytuje dane o zadaniu.
   *
   * @param wejscie - Strumien wejsciowy.
   *
   * @param zad - Zadanie do, ktorego maja zostac wyczytane dane.
   *
   * @retval Strumien wejsciowy.
   *
   * @details Funkcja nie zmienia numery zadania. Trzeba to zrobic samemu.
   */
  friend std::istream & operator >> (std::istream  &wejscie, Zadanie_t &zad)
  { 
    wejscie >> zad._r >> zad._p >> zad._q;
    return wejscie;            
  }

  /**
   * @brief Wypisuje parametry zadania.
   *
   * @param wejscie - Strumien wejsciowy.
   *
   * @param zad - Zadanie do, ktorego maja zostac wyczytane dane.
   *
   * @retval Strumien wejsciowy.
   *
   * @details Nie wypisuje numeru zadania. Trzeba to zrobic samemu.
   */
  friend std::ostream & operator << (std::ostream & wyjscie, const Zadanie_t & zad) {
    wyjscie << "r=" << zad.ret_r() << " p=" << zad.ret_p() << " q=" << zad.ret_q();
    return wyjscie;
  }

private:

  /**
   * @brief Czas przygotowania.
   *
   * Ile musi uplynac czasu od rozpoczecia sekwencji zadan (t=0),
   * zanim bedzie mozna przystapic do wykonywania zadania.
   */
  unsigned int _r;

  /**
   * @brief Czas wykonywania.
   *
   * Ile czasu zajmuje wykonanie samego zadania.
   */
  unsigned int _p;

  /**
   * @brief Czas stygniecia.
   *
   * Ile czasu musi uplynac po wykonaniu samego zadania,
   * zanim uznamy je za ostatecznie zakonczone.
   */
  unsigned int _q;

  /**
   * @brief Numer zadania.
   */
  unsigned int _n;
};

#endif /* ZADANIE_HPP */
