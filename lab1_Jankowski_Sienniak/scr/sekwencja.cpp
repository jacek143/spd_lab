/**
 * @file sekwencja.cpp
 *
 * @brief Klasa reprezentujaca sekwencje zadan.
 */

#include "sekwencja.hpp"

using namespace std;

/**
 * @brief Oblicza czas wykonywania sekwencji zadan.
 *
 * @retval Czas wykonywania sekwencji zadan.
 */ 
unsigned int Sekwencja_t::ret_czas(void) const{
  unsigned int t = 0;
  unsigned int q = 0; // czas stygniecia dla calej sekwencji

  for(unsigned int i = 0; i < this->size(); i++){
      
    unsigned int czas_przestoju = (this->at(i).ret_r() > t) ? (this->at(i).ret_r()-t) : 0;
    t +=  czas_przestoju;
    q -= (czas_przestoju < q) ? czas_przestoju : q;
      
    t += this->at(i).ret_p();
    q -= (this->at(i).ret_p() < q) ? this->at(i).ret_p() : q;

    q = this->at(i).ret_q() > q ? this->at(i).ret_q() : q; 
  } // for

  t += q;

  return t;
}

/**
 * @brief Wypisuje sekwencję zadań.
 *
 * @param wyjscie - Strumien wyjsciowy.
 *
 * @param sekwencja - Wypisywana sekwencja.
 *
 * @retval Strumien wyjsciowy.
 */
ostream & operator << ( ostream & wyjscie, const Sekwencja_t & sekwencja){
  wyjscie << "1 " << sekwencja.size() <<  endl;
  for(unsigned int i = 0; i < sekwencja.size(); i++){
    wyjscie << sekwencja[i].ret_n() << " ";
  } // for
  wyjscie <<  endl;
  wyjscie << sekwencja.ret_czas();
  return wyjscie;
}

/**
 * @brief Wczytuje sekwencję zadań.
 *
 * @param wyjscie - Strumien wejsciowy.
 *
 * @param sekwencja - Zmienna, w której zapisujemy sekwencje.
 *
 * @retval Strumien wejsciowy.
 */
istream & operator >> ( istream & wejscie, Sekwencja_t & sekwencja){ 

  unsigned int liczba_zadan;
  wejscie >> liczba_zadan;
  if(wejscie.fail()){
    return wejscie;    
  }
  
  unsigned int liczba_parametrow; 
  wejscie >> liczba_parametrow;
  if(wejscie.fail()){
    return wejscie;
  }
  // Zawsze beda trzy parametry zadania
  if(liczba_parametrow != 3){
    wejscie.setstate( ios_base::failbit);
    return wejscie;
  }
  
  for (unsigned int i = 0; i < liczba_zadan; i++){
    Zadanie_t zad;
    wejscie >> zad;
    zad.zmien_n(i+1);
    if(wejscie.fail()){
      return wejscie;    
    }
    sekwencja.push_back(zad);
  } // for
  
  return wejscie;
}
