/**
 * @file szeregowanie.hpp
 *
 * @brief Implementacja roznych algorytmow szeregowania
 */

#ifndef SZEREGOWANIE_HPP
#define SZEREGOWANIE_HPP

#include <algorithm>

#include "sekwencja.hpp"

void sort_R(Sekwencja_t & sekwencja);
bool sort_R_cmp(Zadanie_t zad1, Zadanie_t zad2);

void sort_QR(Sekwencja_t & sekwencja);
bool sort_QR_cmp(Zadanie_t zad1, Zadanie_t zad2);

void sort_S(Sekwencja_t & sekwencja);

#endif /* SZEREGOWANIE_HPP */
