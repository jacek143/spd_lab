/**
 * @file szeregowanie.cpp
 *
 * @brief Implementacja roznych algorytmow szeregowania
 */

#include "szeregowanie.hpp"

using namespace std;

/**
 * @brief Implementacja algorytmu sort R.
 *
 * @param sekwencja - Sekwencja, która ma zostać uporządkowana.
 */
void sort_R(Sekwencja_t & sekwencja){
  sort(sekwencja.begin(), sekwencja.end(), sort_R_cmp);
}

/**
 * @brief Implementacja algorytmu sort QR.
 *
 * @param sekwencja - Sekwencja, która ma zostać uporządkowana.
 */
void sort_QR(Sekwencja_t & sekwencja){
  sort(sekwencja.begin(), sekwencja.end(), sort_QR_cmp);
}

struct min_r{
  bool operator ()(const Zadanie_t & zad1, const Zadanie_t & zad2){
  if(zad1.ret_r() > zad2.ret_r()) return true;
  if(zad1.ret_r() < zad2.ret_r()) return false;
  return false;
  }
 };
struct max_q{
  bool operator ()(const Zadanie_t & zad1, const Zadanie_t & zad2){
  if(zad1.ret_q() < zad2.ret_q()) return true;
  if(zad1.ret_q() > zad2.ret_q()) return false;
  return false;
  }
 };

typedef std::priority_queue < Zadanie_t, std::vector<Zadanie_t>, min_r >  TN;
typedef std::priority_queue < Zadanie_t, std::vector<Zadanie_t>, max_q >  TG;

void wstawN(TN & zad, unsigned int r, unsigned int p, unsigned int q, unsigned int n){
  Zadanie_t nowezad(r,p,q,n);
  zad.push(nowezad);
}

void wstawG(TG & zad, unsigned int r, unsigned int p, unsigned int q, unsigned int n){
  Zadanie_t nowezad(r,p,q,n);
  zad.push(nowezad);
}

void sort_S(Sekwencja_t & sekwencja){
  TN N;
  TG G;
  Zadanie_t e;
  unsigned int i=0;
   for(std::vector<Zadanie_t>::iterator it = sekwencja.begin() ; it != sekwencja.end(); ++it ){
     e = sekwencja.at(i);
     i += 1;
     wstawN( N, e.ret_r(), e.ret_p(), e.ret_q(), e.ret_n() );
   }
  sekwencja.clear();
  unsigned int t=0, k=0;
    while( !(G.empty()) || !(N.empty()) ){
      while( !(N.empty()) && N.top().ret_r()<=t ){
  	 e = N.top();
	 wstawG( G, e.ret_r(), e.ret_p(), e.ret_q(), e.ret_n() );
	 N.pop();
      }
      if(G.empty()){
	t = N.top().ret_r();
	  }
	  else{
	    e = G.top();
	    G.pop();
	    k += k;
	    sekwencja.push_back(e);
	    t += e.ret_p();
	  }
   }
}

/**
 * @brief Sprawdza czy pierwsze zadanie ma krotszy czas przygotowania (r) 
 * od zadania drugiego.
 *
 * @param zad1 - Pierwsze zadanie.
 *
 * @param zad2 - Drugie zadanie.
 *
 * @retval Czy pierwsze zadanie ma mniejszy czas  przygotowania (r) 
 * od zadania drugiego.
 */
bool sort_R_cmp(Zadanie_t zad1, Zadanie_t zad2){
  return zad1.ret_r() < zad2.ret_r();
}

/**
 * @brief Sprawdza czy dla zadania nr 1 roznica r-q jest mniejsza niz dla
 * zadania nr 2.
 *
 * @param zad1 - Pierwsze zadanie.
 *
 * @param zad2 - Drugie zadanie.
 *
 * @retval Czy dla zadania nr 1 roznica r-q jest mniejsza niz dla
 * zadania nr 2.
 */
bool sort_QR_cmp(Zadanie_t zad1, Zadanie_t zad2){
  return ((int) zad1.ret_r() - (int) zad1.ret_q()) < ((int) zad2.ret_r() - (int) zad2.ret_q());
}
