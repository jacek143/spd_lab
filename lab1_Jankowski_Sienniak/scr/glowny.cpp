/**
 * @file glowny.c
 *
 * @brief Plik zwaierajacy funkcje main().
 *
 * @author Jacek Jankowski
 *
 * @date 27.02.2015
 */

#include <iostream>
#include <fstream>

#include "zadanie.hpp"
#include "sekwencja.hpp"
#include "szeregowanie.hpp"

using namespace std;

/**
 * @brief Funkcja main zawierajaca 'rdzen' programu.
 *
 * Dane wejjciowe programu:
 * 1) nazwa pliku zawierajacego opis zadan
 * 2) nazwa pliku wyjsciowego, ktory ma zawierac sekwencje zadan - ten argument jest opcjonalny
 *
 * Dane wyjsciowe programu:
 * 1) Proponowana sekwencja zadan oraz jej dlugosc
 *
 * Dzialanie programu:
 * 	1. Pobierz nazwe pliku z opisem zadan
 * 	2. Jezli nie udalo sie uzyskac nazwy, wyswietl ostrzezenie i zakoncz program.
 *	2. Sprobuj wczytac dane z pliku wejsciowego.
 *	3. Jezli nie udalo sie wczytac danych lub dane sa niepoprawne,
 *		to wyswietl ostrzezenie i zakoncz program.
 *	4. Znajdz mozliwie najlepsza sekwencje zadan.
 *	5. Oblicz dlugosc otrzymanej sekwencji.
 *	6. Wyswietl sekwencje i jej dlugosc na terminalu.
 *	5. Pobierz nazwe pliku wyjsciowego.
 *	6. Jezeli uzytkownik nie podal nazwy pliku wyjsciowego, to zakoncz dzialanie programu.
 *	7. Sprobuj zapisac sekwencje i jej dlugosc w pliku wyjsciowym.
 *	8. Jezli nie udalo sie zapisac danych,
 *		to wyswietl ostrzezenie i zakoncz program.
 *	9. Zakoncz dzialanie programu.
 *
 *	@details Dane wejsciowe podajemy za pomoca argumentow wywolania.
 *
 *	@retval Jezeli nie napotkano zadnego bledu, to zwracamy 0. W przeciwnym razie zwracamy 1.
 */
int main(int argc, char **argv) {
  Sekwencja_t sekwencja;

  if(argc < 2){
    cerr << "Blad. Nie podano nazwy pliku z opisem zadan." << endl;
    return 1;
  }
  
  ifstream plik_wej(argv[1]);
  if(plik_wej.fail()){
    cerr << "Blad. Nie udalo sie otworzyc pliku z opisem zadan." << endl;
    return 1;
  }

  const char * wiad_plik_zly = 
    "Blad. Plik z opisem zadan jest uszkodzony lub zawiera niepoprawne dane.";  

  plik_wej >> sekwencja;

  if(plik_wej.fail()){
    cerr <<  wiad_plik_zly << endl;
    return 1;
  }

  plik_wej.close();
  
  sort_QR(sekwencja);

  cout << sekwencja << endl;

  if(not (argc > 2)){
    return 0;
  }

  ofstream plik_wyj(argv[2]);
  if(plik_wyj.fail()){
    cerr << "Blad. Nie udalo sie otworzyc pliku do zapisania wynikow." << endl;
    return 1;
  }

  plik_wyj << sekwencja;
  if(plik_wyj.fail()){
    cerr << "Blad. Zapis wynikow do pliku nie powiodl sie." << endl;
    plik_wej.close();
    return 1;    
  } 

  return 0;
}




