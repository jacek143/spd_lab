/**
 * @file sekwencja.hpp
 *
 * @brief Klasa reprezentujaca sekwencje zadan.
 */

#ifndef SEKWENCJA_HPP
#define SEKWENCJA_HPP

#include <vector>
#include <queue>
#include <iostream>

#include "zadanie.hpp"

/**
 * @brief Klasa reprezentujaca sekwencje zadan.
 */
class Sekwencja_t : public std::vector<Zadanie_t> {

public:

  unsigned int ret_czas(void) const;

  friend std::ostream & operator << (std::ostream & wyjscie, const Sekwencja_t & sekwencja);

  friend std::istream & operator >> (std::istream & wejscie, Sekwencja_t & sekwencja);
};

#endif /* SEKWENCJA_HPP */
