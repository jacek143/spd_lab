#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>

#define INFINITY  100000000
using namespace std;

class zadanie{
	public:
		zadanie(int s, int k)
		{
			suma=s;
			kolejnosc=k;
		}
		unsigned ret_suma() {
			return suma;	
		}
		
		unsigned ret_kolejnosc(){
			return kolejnosc;
		}
	private:
		unsigned suma;
		unsigned kolejnosc;
};

class zadanie_rpq{
		public:
		zadanie_rpq(unsigned r, unsigned p, unsigned q)
		{
			_r=r;
			_p=p;
			_q=q;
		}
		unsigned ret_r() {
			return _r;	
		}
		unsigned ret_p(){
			return _p;
		}
		unsigned ret_q(){
			return _q;
		}
		void set_r(unsigned r)
		{
			_r=r;
		}
		void set_q(unsigned q)
		{
			_q=q;
		}
	private:
		unsigned _r;
		unsigned _p;
		unsigned _q;
};

vector<vector<zadanie_rpq> > tablica;
vector<zadanie_rpq> tablicapom;
vector<zadanie_rpq> tablicapom2;
unsigned liczba_maszyn;
clock_t start;
clock_t suma_czas;

bool porownanie(zadanie zad1, zadanie zad2){
	if(zad1.ret_suma()==zad2.ret_suma()) return zad1.ret_kolejnosc()>zad2.ret_kolejnosc();
	return zad1.ret_suma()<zad2.ret_suma();
}

int licz_cmax(std::vector<zadanie> wejsciowa, int **tab)
{
	vector<int> c(liczba_maszyn+1,0);
	for(std::vector<zadanie>::iterator it=wejsciowa.begin(); it!=wejsciowa.end(); it++){
		for(unsigned i=0; i<liczba_maszyn; i++){
			c[i+1]=max(c[i],c[i+1])+tab[it->ret_kolejnosc()-1][i];
		}
	}
	return c[liczba_maszyn];
}

void licz_rq(unsigned l)
{
	vector<unsigned> c(liczba_maszyn+1,0);
	for(int j=l; j<tablica.size(); j++){
		for(int i=0; i<liczba_maszyn; i++){
			if(l!=0) c[i+1] = tablica[j-1][i].ret_r(); 
			c[i+1]=max(c[i],c[i+1])+tablica[j][i].ret_p();
			tablica[j][i].set_r(c[i+1]);
		}
	}
	
	vector<unsigned> d(liczba_maszyn+1,0);
	for(int j=l; j>=0; j--){
		for(int i=liczba_maszyn-1; i>=0; i--){
			if(l!=tablica.size()-1) d[i] = tablica[j+1][i].ret_q();
			d[i]=max(d[i],d[i+1])+tablica[j][i].ret_p();
			tablica[j][i].set_q(d[i]);
		}
	}
}

void licz_rq_pom()
{
	vector<unsigned> c(liczba_maszyn+1,0);
		for(int i=0; i<liczba_maszyn; i++){
			c[i+1]=max(c[i],c[i+1])+tablicapom[i+1].ret_p();
			tablicapom[i+1].set_r(c[i+1]);
		}
	
	vector<unsigned> d(liczba_maszyn+1,0);
		for(int i=liczba_maszyn-1; i>=0; i--){
			d[i]=max(d[i],d[i+1])+tablicapom[i+1].ret_p();
			tablicapom[i+1].set_q(d[i]);
		}
		tablicapom2=tablicapom;
}

int licz_cmax_akceleracja(unsigned k)
{
	unsigned cmax=0;
	unsigned temp;
	for(unsigned i=0; i<liczba_maszyn; i++){
		if(k==0) {
		tablicapom=tablicapom2;
		temp = tablicapom[i+1].ret_r() +  tablica[k][i].ret_q();
		}	
		if(k==tablica.size()) {
		tablicapom=tablicapom2;
		temp = tablica[k-1][i].ret_r() + tablicapom[i+1].ret_q();
		}
		if(k>0 && k<tablica.size()) {
			tablicapom[i+1].set_r(max(tablicapom[i].ret_r(),tablica[k-1][i].ret_r())+tablicapom[i+1].ret_p());
			temp = tablicapom[i+1].ret_r() + max( tablica[k][i].ret_q(), tablicapom[i+2].ret_q() );
		}
		if(temp>cmax) cmax=temp;
	}
	return cmax;
}


int main()
{
	start = 0;
	suma_czas = 0;
	const string plik="neh.data.txt";
	fstream h_plik(plik.c_str());		
	for(unsigned x=0; x<=120; x++){		//G��wna p�tla
	unsigned n, opt_cmax, opt_sek, temp, temp2; //Wczytywanie pliku /*
	string nazwa;
	h_plik >> nazwa;
	h_plik >> n >> liczba_maszyn;
	cout << endl << nazwa << endl;
	
	int **tab = new int *[n];
  	for(int i=0;i<n;i++)
    tab[i] = new int [liczba_maszyn];
	vector<zadanie> wielkosci;
	vector<zadanie> neh;
	vector<zadanie> nehpom;
		
	for(unsigned i=0; i<n; i++){
		temp=0;
		for(unsigned j=0; j<liczba_maszyn; j++){
			h_plik >> tab[i][j];
			temp+=tab[i][j];	
		} 
		wielkosci.push_back(zadanie(temp,i+1));
	}
	
	h_plik >> nazwa;
	h_plik >> opt_cmax;
	cout << nazwa << "\t\t" << opt_cmax << endl;
	for(unsigned i=0; i<n; i++){
		h_plik >> opt_sek;
		//cout << opt_sek << " ";		//Wypisywanie sekwencji z pliku
	} 	//Wczytywanie pliku */
	//cout << endl;
	
	start = clock();
	sort(wielkosci.begin(), wielkosci.end(), porownanie);	//sortowanie

	unsigned cmax=1, l;
	for(unsigned i=0; i<n; i++){	//NEH
		if(neh.empty()){
			neh.push_back(wielkosci.back());
			tablicapom.clear();
			for(unsigned z=0; z<liczba_maszyn; z++) tablicapom.push_back(zadanie_rpq(0,tab[wielkosci.back().ret_kolejnosc()-1][z],0));
			tablica.push_back(tablicapom);
			wielkosci.pop_back();
			//nehpom=neh;
			licz_rq(0);
		}
		else {
			//neh.push_back(wielkosci.back());
			tablicapom.clear();
			for(unsigned z=0; z<liczba_maszyn; z++) tablicapom.push_back(zadanie_rpq(0,tab[wielkosci.back().ret_kolejnosc()-1][z],0));
			//wielkosci.pop_back();
			//nehpom=neh;
			unsigned ccmax=INFINITY;
			unsigned k=neh.size()+1;

			tablicapom.insert(tablicapom.begin(),zadanie_rpq(0,0,0));
			tablicapom.push_back(zadanie_rpq(0,0,0));
			tablicapom2=tablicapom;
			licz_rq_pom();
			
			for(unsigned j=1; j<=neh.size()+1; j++){
				//temp = licz_cmax(nehpom, tab);
				
				temp = licz_cmax_akceleracja(k-1);
				
				if(temp<=ccmax){
					ccmax=temp;
					cmax=ccmax;			
					//neh=nehpom;
					l=k-1;
				}
				if(j<neh.size()+1) {
				//swap(nehpom[nehpom.size()-j],nehpom[nehpom.size()-j-1]);
				k--;
				}
				
			} 	//FOR
			tablicapom.pop_back();
			tablicapom.erase(tablicapom.begin());	
			tablica.insert(tablica.begin()+l,tablicapom);
			neh.insert(neh.begin()+l,wielkosci.back());
			wielkosci.pop_back();
			
			licz_rq(l);
		}	//ELSE
	}	//NEH
	
	cout << "Obliczone:  ";
	cout << "\t" << cmax << endl;
	/*for(std::vector<zadanie>::iterator it=neh.begin(); it!=neh.end(); it++){	
		cout << it->ret_kolejnosc() << " ";
	}
	cout << endl;*/
	tablica.clear();
	suma_czas = suma_czas + (clock() - start);
	}//G�owna p�tla
	
	cout << "czas algorytmu bez wczytywania danych: " << suma_czas << "[ms]";
	return 0;
}
