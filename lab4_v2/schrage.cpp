#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <vector>

using namespace std;

unsigned schrage_podzial(const unsigned n, const unsigned r[], const unsigned p[], const unsigned q[]);

unsigned schrage(const unsigned n, const unsigned r[], const unsigned p[], const unsigned q[], unsigned pi[]);

/**
* @brief G��wna funkcja odpowiedzialna za sprawdzenie poprawnego dzia�ania algorytmu.
*/
int main(int argc, char** argv) {
	const string nazwa_pliku = "schr.data.txt"; // nazwa pliku wejsciowego
	const unsigned N = 50; // maksymalna ilosc zadan
	unsigned r[N], p[N], q[N]; // czasy
	unsigned pi[N]; // miejsce dla uporz�dkowanej permutacji 
	unsigned n; // liczba zadan
	string smietnik; // w pliku znajduja sie niepotrzebne dla nas informacje
	ifstream plik;
	
	plik.open(nazwa_pliku.c_str());

	for(unsigned j = 0; j < 9; j++){
		do{
			plik >> smietnik;
		}while(smietnik[0] != 'd');
		plik >> n;
		for(unsigned i = 0; i < n; i++){
			plik >> r[i] >> p[i] >> q[i];
		}
		cout << "Schr:" << endl;
		cout << schrage(n, r, p, q, pi) << endl;
		for(unsigned i = 0; i < n; i++){
			cout << pi[i] << " ";
		}
		cout << endl << endl;
	}
	
	plik.close();
	
	//system("pause");
	return 0;
}

unsigned schrage_podzial(const unsigned n, const unsigned r[], const unsigned p[], const unsigned q[]){
	unsigned t; // chwila czasowa,
	unsigned Cmax;
	list<unsigned> N; // zbi�r zadan nieuszeregowanych
   	list<unsigned> G; // zbi�r zadan gotowych do realizacji
   	list<unsigned> zrobione; // zbi�r zadan przetworzonych
   	unsigned e;
	vector<unsigned> stopien_przetworzenia(n, 0);
	
	t = 0; Cmax = 0; G.clear();
   	for(unsigned i = 0; i < n; i++){
		N.push_back(i+1); // zadania numerujemy od 1
	} // for

	while(G.size() or N.size()){
		for(list<unsigned>::iterator it = N.begin(); it != N.end(); it++){
			if(r[(*it)-1] <= t){
				G.push_back(*it);
				N.erase(it);
				it = N.begin();
			}
		} // for
		
		t++;
		if(G.empty()){
		}
		else{
			unsigned i_qmin = *(G.begin());
			for(list<unsigned>::iterator it = G.begin(); it != G.begin(); it++){
				if(q[i_qmin] > q[*it]){
					i_qmin = *it;
				} // if
			} // for
			stopien_przetworzenia[i_qmin] += 1;
			if(stopien_przetworzenia )
		}
		
		/*
		for(list<unsigned>::iterator it = N.begin(); it != N.end(); it++){
			if(r[(*it)-1] <= t){
				G.push_back(*it);
				N.erase(it);
				it = N.begin();
			}
		} // for
		if(G.empty()){
			t = r[(*(N.begin()))-1];
			for(list<unsigned>::iterator it = N.begin(); it != N.end(); it++){
				if(t > r[(*it)-1]){
					t = r[(*it)-1];
				}
			} // for
		}
		else{
			list<unsigned>::iterator it_maxq = G.begin();
			for(list<unsigned>::iterator it = G.begin(); it != G.end(); it++){
				unsigned tmp = *it;
				if(q[(*it)-1] > q[(*it_maxq)-1]){
					it_maxq = it;
				}
			} // for
			unsigned e = *it_maxq;
			G.erase(it_maxq);
			pi[k++] = e;
			t += p[e-1];
			Cmax = (Cmax >= (t + q[e-1])) ? Cmax : (t+q[e-1]);
		}*/
	} // while
	
	return Cmax;
}

/**
* @brief Algorytm Schrage.
* 
* @param n - liczba zada�
*
* @param r - terminy dostepnosci zada�
*
* @param p - czasy wykonania zada�
*
* @param q - czasy dostarczenia zada�
*
* @param pi - permutacja wykonania zadan na maszynie,
*
* retval Cmax - maksymalny z termin�w dostarczenia zadan
*/
unsigned schrage(const unsigned n, const unsigned r[], const unsigned p[], const unsigned q[], unsigned pi[]){
	unsigned t; // chwila czasowa,
	unsigned Cmax;
	unsigned k; // pozycja w permutacji
	list<unsigned> N; // zbi�r zadan nieuszeregowanych
   	list<unsigned> G; // zbi�r zadan gotowych do realizacji
   	unsigned e;
   	
   	t = 0; k = 0; Cmax = 0; G.clear();
   	for(unsigned i = 0; i < n; i++){
		N.push_back(i+1); // zadania numerujemy od 1
	} // for
	
	while(G.size() or N.size()){
		for(list<unsigned>::iterator it = N.begin(); it != N.end(); it++){
			if(r[(*it)-1] <= t){
				G.push_back(*it);
				N.erase(it);
				it = N.begin();
			}
		} // for
		if(G.empty()){
			t = r[(*(N.begin()))-1];
			for(list<unsigned>::iterator it = N.begin(); it != N.end(); it++){
				if(t > r[(*it)-1]){
					t = r[(*it)-1];
				}
			} // for
		}
		else{
			list<unsigned>::iterator it_maxq = G.begin();
			for(list<unsigned>::iterator it = G.begin(); it != G.end(); it++){
				unsigned tmp = *it;
				if(q[(*it)-1] > q[(*it_maxq)-1]){
					it_maxq = it;
				}
			} // for
			unsigned e = *it_maxq;
			G.erase(it_maxq);
			pi[k++] = e;
			t += p[e-1];
			Cmax = (Cmax >= (t + q[e-1])) ? Cmax : (t+q[e-1]);
		}
	} // while
	return Cmax;
}

